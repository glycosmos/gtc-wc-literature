import {LitElement, html, css } from 'lit-element';
import getdomain from 'gly-domain/gly-domain.js';


class GtcWcLiterature extends LitElement {
  static get properties() {
    return {
      sampleids: Array,
      accession: String,
      toggle: Object
    };
  }
  static get styles() {
    return css `
    main {
    	border: 0;
    	border-radius: 0;
    	box-shadow: none;
    	overflow: hidden;
    }

    .literatureList {
    	margin: 10px 0 0;
    	padding: 0;
    	font-size: 14px;
    	list-style-type: none;
    }

    .literatureList > li.stanzaNothingFound {
    	width: 100%;
    	float: none;
    	margin: 0 0 10px;
    	padding: 5px 0;
    	background: #EEE;
    	color: #999;
    	font-size: 14px;
    	font-weight: bold;
    	text-align: center;
    }

    .literatureList > li:before, .literatureList > li:after {
    	content: " ";
    	display: table;
    }

    .literatureList > li:after {
    	clear: both;
    }

    .literatureList > li + li {
    	margin: 20px 0 0;
    	padding: 0;
    }

    .literatureList_category {
    	width: 100%;
    	margin: 0;
    	float: left;
    	list-style-type: disc;
    }

    .literatureList_category:before, .literatureList_category:after {
    	content: " ";
    	display: table;
    }

    .literatureList_category:after {
    	clear: both;
    }

    .literatureList_category > li {
    	min-width: 100px;
    	margin: 0 15px 0 0;
    	float: left;
    	line-height: 2;
    }

    .literatureList_category > li > a {
    	color: #09D;
    	text-decoration: underline;
    }


    /* Register publication*/
    .entryNew_section {
    	margin: 0 0 20px;
    	padding: 0 0 30px;
    }

    .entryNew_heading {
    	margin: 0;
    	padding: 0 0 5px;
    	border-bottom: solid 1px #999;
    	font-size: 18px;
    	line-height: 1.2;
    	font-weight: bold;
    }

    .entryNew_heading-2nd {
    	margin: 20px 0 0 30px;
    	padding: 10px 0 0;
    	border-bottom: 0;
    	font-size: 16px;
    	font-weight: bold;
    }

    /* Partner Explanation */
    .source {
    	margin: 15px 0 0;
    	padding: 0;
    	text-align: right;
    	font-size: 14px;
    }

    .source_content {
    	display: none;
    	margin: 5px 0 10px;
    	padding: 20px;
    	background: #FFC;
    }

    .source_content--show {
    	display: block;
    }

    .source_text {
    	margin: 0;
    	padding: 0;
    	text-align: left;
    	color: #787878;
    	font-size: 14px;
    }

    .source_btn {
    	margin: 0 0 0 auto;
    	width: 150px;
    	padding: 0;
    	padding: 5px 0;
    	text-align: left;
    	font-size: 14px;
    }

    .source_btn:hover {
    	background-color: #EEE;
    }

    .toggleBox_label {
      cursor: pointer;
    }

    .toggleBox_checkbox {
      display: none;
    }

    .toggleBox_checkbox + .toggleBox_content {
      display: none;
    }

    .toggleBox_checkbox:checked + .toggleBox_content {
      display: block;
    }
    .nothingFound {
      width: 100%;
      float: none;
      margin: 0 0 10px;
      padding: 5px 0;
      background: #EEE;
      color: #999;
      font-size: 14px;
      font-weight: bold;
      text-align: center;
      box-sizing: border-box;
    }
    `;
  }

  render() {
    return html `

    <div>${this._processHtml()}<div>

   `;
  }


  constructor() {
    super();
    console.log("constructor");
    this.accession="";
    this.image="";
    this.sampleids=[];
    this.mass=null;
    this.contributionTime=null;
  }

  connectedCallback() {
    super.connectedCallback();
    console.log("cc");
    const host =  getdomain(location.href);
    const url1 = 'https://' + host + '/sparqlist/api/gtc_literature?accNum=' + this.accession;
    this.gteContents(url1);
  }

  gteContents(url1) {
    console.log(url1);
    var urls = [];

    urls.push(url1);
    var promises = urls.map(url => fetch(url, {
      mode: 'cors'
    }).then(function (response) {
      return response.json();
    }).then(function (myJson) {
      console.log("literature");
      console.log(JSON.stringify(myJson));
      return myJson;
    }));
    Promise.all(promises).then(results => {
      console.log("values");
      console.log(results);

      // this.sampleids = results.pop();
      this.sampleids = results;
      console.log("sampleids");
      console.log(this.sampleids[0]);
      console.log(Object.keys(this.sampleids[0]));
      console.log(Object.keys(this.sampleids[0]).length);
      // var demo = JSON.stringify(this.sampleids);
      // console.log(demo);
    });
  }

  _processHtml() {
    if (Object.keys(this.sampleids[0]).length) {
      var db_hash = this.sampleids[0];
      return html `
        <ul class="literatureList">
          ${Object.keys(db_hash).map( db => html`
          <li>
            <ul class="literatureList_category">
              ${db_hash[db].map(item => html`
              <li><a href=${item.url} target="_blank">${item.id}</a></li>
              `)}
            </ul>
            <div class="source toggleBox">
              <label for="${db}" class="source_btn toggleBox_label">from ${db}</label>
              <input id="${db}" class="toggleBox_checkbox" type="checkbox" />
            </div>
          </li>
          `)}
        </ul>
      `;
    } else {
      return html`<div class="nothingFound">Nothing found in this entry.</div>`;
    }
  }

}

customElements.define('gtc-wc-literature', GtcWcLiterature);
